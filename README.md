# President Handbook

Handbook of the key things that the VECTOR president needs to remember.  

## Introduction

`mdbook` replaces the original `gitbook-legacy` method for deploying the page on gitlab

## Features

* Easy to edit on Gitlab or at home using git
* Files formatted in simple Markdown
* Gitlab CI-CD system generates and publishes the web pages after every push.

## Contents

* [What is this?](#what-is-this)
* [Avoid preset docker images](#avoid-preset-docker-images)
* [Used Technologies](#used-technologies)
* [Getting Started](#getting-started)
    * [Requirements](#requirements)
    * [Install](#install)
    * [Usage](#usage)
* [Resources](#resources)
* [Contribute](#contribute)
* [License](#license)

## What is this?

This is a simple way for the VECTOR president to maintain a public handbook that they can share with other members.

## Avoid preset docker images

Preset docker images distracted me for a bit:
- peaceiris/mdbook
- chainguard/mdbook

You need to heavily edit either one for standalone use.  Chainguard is set up for their infrastructure.  PeaceIris is set up for a particular way of doing things.  

## Used Technologies

* Markdown
* mdbook
* mdbook-toc
* toml
* yaml

## Getting Started

### Requirements

* Basic knowledge of git
* Basic knowledge of [Markdown][about-markdown] (here is a [Cheat Sheet][markdown-cheatsheet])

### Install

Use git to clone this repository into your computer.

```
git clone https://gitlab.com/ve7det/president-handbook
```

### Usage

Edit the content with your favourite editor and follow Markdown formatting.  

The files stored in `src` are the book content.  


## Resources

1. [Yethiel's mdbook Pages template](https://gitlab.com/yethiel/pages-mdbook)
2. [YAML checker](https://yamlchecker.com)
3. [Gitlab docs Configuring CI](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)
4. [Gitlab docs on Yaml](https://docs.gitlab.com/ee/ci/yaml/index.html)
5. [About Markdown][about-markdown]
6. [Markdown Cheat Sheet][markdown-cheatsheet]

[about-markdown]: https://markdownguide.org/getting-started/
[markdown-cheatsheet]: https://markdownguide.org/cheat-sheet/

## Contribute

Pull requests are welcome.  For major changes, issues allow us to discuss first what you wish to change.  

Please make sure to update tests as appropriate.  

## License

[CC BY-NC](https://creativecommons.org/licenses/by-nc/4.0/)

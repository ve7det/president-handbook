# Introduction

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [About This Handbook](#about-this-handbook)
- [Structure](#structure)
- [Other Handbooks](#other-handbooks)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

<!-- DOCTOC SKIP -->

If you seek introductory information about VECTOR, the Vancouver Emergency Community Telecommunications Organization, check our website [vectorradio.ca](https://vectorradio.ca).  

## About This Handbook

This Handbook's primary audience is presidents of VECTOR, but this version is publicly available for others who might find it useful.  The goal is to give everyone in the organization as much clarity and context as possible while sharing these practices with members considering a role on the board and the broader community as well.  

This book started in 2022 and is constantly changing.  If you see something that could improve: that's your invitation to improve it.  [Find out more about editing the handbook](guides/contributing.md).  

## Structure

The book is broken up into sections.  The sections are:  

* [**Executive**](executive/) describes work to keep the organization going.  
* [**O&I Forum**](oiforum/) explains the Operations and Intelligence Forum and Members Meeting.  The president facilitates these sessions.  
* [**Governance**](governance/) explains the Governance Meeting that supports the organization.  The president facilitates these sessions.  
* [**Teams**](teams/) describes work to support teams within the organization so we can keep ready to meet our commitment for disaster response.  Teams are where members do work on our equipment and programmes.  
* [**Community**](community/) describes work to build partnerships with our community so we can serve that community.  
* [**Policies**](policies/) the official rules that govern members.  Changing these requires a formal process.  

## Other Handbooks

The information in this handbook pertains to the president of VECTOR.  VECTOR maintains other handbooks suitable for individual volunteers and for our community response efforts.  

* VECTOR Activation Guide
* VECTOR Member Handbook

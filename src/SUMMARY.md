<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Table of contents](#table-of-contents)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Table of contents

<!-- DOCTOC SKIP -->

* [Introduction](README.md)
* [Overview](overview.md)
* [Executive](executive/README.md)
    * [Charity and MOU](executive/context.md)
    * [Responsibilities](executive/responsibilities.md)
    * [Preparing for the Role](executive/preparing.md)
    * [Resources](executive/resources.md)
* [O&I Forum](oiforum/README.md)
    * [Forum Elements](oiforum/oifelements.md)
    * [Example Agenda](oiforum/oifagenda.md)
    * [Example Meeting Record](oiforum/oifrecord.md)
* [Governance](governance/README.md) 
    * [Governance Elements](governance/govelements.md)
    * [Example Agenda](governance/govagenda.md)
    * [Example Meeting Record](governance/govrecord.md)
* [Community](community/README.md) 
    * [Outreach](community/outreach.md)
    * [Partnership](community/partnership.md)
    * [Building Capacity](community/capacity.md)
* [Guides](guides/README.md)
    * [Contributing](guides/contributing.md)
    * [Meeting Participation](guides/meetings.md)
    * [Meeting Principles](guides/meeting-principles.md)
    * [Meeting Records](guides/meeting-records.md)
    * [Meeting Preparation](guides/meeting-prep.md)
    * [Team Huddles](guides/teamhuddles.md)
    * [Ladder of Leadership](guides/ladder.md)
    * [O&I Forum Format](guides/oifformat.md)
    * [Governance Session Format](guides/govformat.md)
    * [Directors Role](guides/directors.md)
    * [Annual Transition](guides/transition.md)
    * [Practising Brevity](guides/brevity.md)
    * [Key Reference Books](guides/core-library.md)
* [Policies](policies/README.md)
    * [Finance Policy](policies/README.md#finance-policy)
    * [Community Code of Conduct](policies/README.md#community-code-of-conduct)
    * [Volunteer Code of Conduct](policies/README.md#volunteer-code-of-conduct)
    * [EMBC Volunteer Code of Conduct](policies/README.md#embc-code-of-conduct)
    * [Confidentiality Agreement](policies/README.md#confidentiality-agreement)

# Community

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Community Centres](#community-centres)
- [Community Centre Associations](#community-centre-associations)
- [Earthquake and Emergency Preparedness Groups](#earthquake-and-emergency-preparedness-groups)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Community Centres

Supporting the disaster response in Vancouver includes the City Emergency Support Services plan and any sites that ESS may need to use.  Community Centres form natural support sites.  

VECTOR placed multi-radio grab and go kits and antennas at "go-to" community centres.  With growing capacity, VECTOR began re-building relationships with Vancouver Parks Board recreation staff.  

Vancouver communities must be able to rely on local VECTOR members to make sure that equipment around the city remains ready.  Building rapport between local members and the community centre staff is key.  

## Community Centre Associations

Community Centre Associations could form the backbone of community response efforts.  These are community non-profit organizations with close relationship to the communities around community centres.  

Community Centre Associations run the education and sports programs within the physical community centres that Parks Board maintains.  

Kerrisdale Community Centre Society has worked with VECTOR to equip a radio kit for the Kerrisdale Earthquake and Emergency Preparedness group.  Fostering this partnership gives both local ESS volunteers and local VECTOR members opportunities for growth in technical and support abilities.  

## Earthquake and Emergency Preparedness Groups

By 2020, City of Vancouver hosts two community response groups committed to making [Disaster Support Hubs][dsh] a reality.  

Both Dunbar and Kerrisdale use the [Neighbourhood Resilience Toolkit][nrtk] to build capacity for community disaster response.  Each bring together volunteers, community centre staff, and business improvement associations to build community resilience capacity.  

[dsh]: https://vancouver.ca/home-property-development/disaster-support-hubs.aspx

[nrtk]: https://vancouver.ca/people-programs/resilient-neighbourhoods-program.aspx#toolkit

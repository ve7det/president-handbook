# Context

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Key Principles](#key-principles)
- [Charitable Organization](#charitable-organization)
- [Registered Society in the Province of British Columbia](#registered-society-in-the-province-of-british-columbia)
- [Memorandum of Understanding](#memorandum-of-understanding)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Key Principles

Serving as the president involves several key principles that arise from VECTOR's context as a Canadian charity operating in British Columbia to serve the City of Vancouver.  

1. serve charitable purposes 
2. spend at least the minimum amount on our purpose
3. remember our registered constitution and bylaws
4. offer emergency and preparedness two-way radio communications for voice and data transmission, equipment, personnel to help meet the business requirements of the COV and VPD


## Charitable Organization

Rules for Canadian charities and non-profit societies in British Columbia govern VECTOR's activity in the community.  Both carry responsibilities to balance privileges.  

Charity
: Organizations registered with Canada Revenue Agency that can issue tax receipts for donations to serve a combination of the four charitable purposes (refer to [website][charity])  

>Registered charities are charitable organizations, public foundations, or private foundations that are created and resident in Canada. They must use their resources for charitable activities and have charitable purposes that fall into one or more of the following categories:
>
> * the relief of poverty
> * the advancement of education
> * the advancement of religion
> *  other purposes that benefit the community

[charity]: https://www.canada.ca/en/revenue-agency/services/charities-giving/charities/operating-a-registered-charity.html

Find checklists and resources for operating Canadian charitable organizations at the [CRA website][charity].  

## Registered Society in the Province of British Columbia

VECTOR maintains status as a registered society in the province of British Columbia that forms the foundation of our registration as a Canadian charitable organization.  

Find further details about registered societies in BC at the [website][societies] or in the [act and regulations][socact].  

Refer to our Constitution and Bylaws for further information.  

[societies]: https://www2.gov.bc.ca/gov/content/employment-business/business/not-for-profit-organizations

[socact]: https://www.bclaws.ca/civix/content/complete/statreg/1527898742/15018/?xsl=/templates/browse.xsl

## Memorandum of Understanding

Along with memorandums of understanding with several other amateur radio organizations to support local repeaters, VECTOR maintains a memorandum of understanding with our City partners to define and protect our working relationship.  

The Memorandum of Understanding (MOU) between the City of Vancouver, Vancouver Police Department and VECTOR serves three purposes, listed below, while outlining roles and responsibilities for each party as well.  

Purposes of the MOU:  

> * provide a framework for service delivery and working relationship between the COV, VPD and VECTOR.  The framework for service delivery establishes and maintains a service coordination structure in support of operational readiness, community and business resilience and response, recovery, and event operations, and provides unique protocols where necessary for each service provided by VECTOR to the COV and the VPD;
>
> * identify VECTOR services and outline the process for VECTOR to deliver those services to the COV and VPD; and
>
> * address a long term relationship and partnership between the COV, VPD and VECTOR to offer emergency and preparedness two‐way radio communications for voice and data transmission, equipment, personnel and other emergency communication services essential to meet the business requirements of the COV and VPD.
# Responsibilities as President

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Director of a Charity](#director-of-a-charity)
- [Director of a Registered Society](#director-of-a-registered-society)
- [Leadership](#leadership)
  - [Leader's Assumptions](#leaders-assumptions)
  - [Principles and practices](#principles-and-practices)
- [Annual Tasks](#annual-tasks)
- [Transition Tasks](#transition-tasks)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Director of a Charity

Under law, you ensure your charitable organization: 

* Performs only allowable activities
* Identifies and spends minimum amount
* Issues complete and accurate donation receipts
* Keeps adequate books and records
* Maintains status as a legal entity
* Files annual information return with Canada Revenue Agency

Find detailed checklists on the [CRA website][checklists].

[tools]: https://www.canada.ca/en/revenue-agency/services/charities-giving/charities/operating-a-registered-charity/toolbox-directors-officers-volunteers.html

[checklists]: https://www.canada.ca/en/revenue-agency/services/charities-giving/charities/checklists-charities.html

## Director of a Registered Society

Under law you ensure your registered society (legal entity):

* carries on business only to advance or support the purposes of the society without profit or gain
* keeps records noted in section 20 of the Societies Act
* operates within the law, especially the Societies Act and Regulations

Actions: 

* manage, or supervise the management of, the activities and internal affairs of the society

* act honestly and in good faith with a view to the best interests of the society

* exercise the care, diligence and skill that a reasonably prudent individual would exercise in comparable circumstances

* act in accordance with the Societies Act, Societies Regulations and the bylaws of the society

* act with a view to the purposes of the society

* act to maintain duty and trust within the society

Find the full text of the Societies Act and Regulations [online][socact].  


[socact]: https://www.bclaws.ca/civix/content/complete/statreg/1527898742/15018/?xsl=/templates/browse.xsl

## Leadership

Consider your roles in a network of self-organizing teams: [thoughts about leading][insights] (25 minutes).  Think in granular roles rather than large, encompassing jobs.  

Roles: 

* public face of organization
* sensing what is needed and offering vision to organization ([Koenig's Source Work][koenig])
* giving advice to teams and members seeking guidance for organization decisions (in advice process)
* holding the space for self-organizing teams
* role model for self-organizing breakthroughs and principles, humility, authenticity, right to failure, possibility to correct things
* invite people to take over and deliver on visions
* create context in which other people can make decisions


Embody the principles below, practice the advice process, and use the conflict mechanism.  

### Leader's Assumptions

People:

* are creative, thoughtful, trustworthy, reliable, self-motivated, intelligent adults, capable of making important decisions
* are accountable and responsible for their decisions and actions
* are fallible
* are unique
* want to use our talents and skills to make a positive contribution to the organization and the world

Performance depends on happiness, which comes from freedom to decide how and understanding why and for whom we work

Value is created by our members.


### Principles and practices

* Purpose
    * Think long term
    * Listen to VECTOR's calling to purpose ([sense and respond][purpose] to emerging conditions)
    * Practice [source work][source] principles
* Self organization
    * Trust and support members to act with positive intent and accountability
    * Keep information and decision-making open to all members unless individual privacy demands otherwise
    * Help members become comfortable holding others accountable to commitments, through feedback and respectful confrontation
    * Give control; avoid giving orders; leave room for questioning
    * Have conversations; focus on people
    * Reduce monitoring and inspection points
    * Pass information
    * Certify rather than brief teams
    * Eliminate steps and processes that don't add value
* Wholeness
    * Strive to create caring safer spaces to serve VECTOR in
    * Treat all members with equal worth
    * Strive to recognize wholeness and interconnection
    * Treat every situation as in invitation to learn and grow -- we have never arrived
    * Hold low-repetition, high-quality training
* Relationships
    * Strive to protect relationships
    * Listen more than you speak
    * Take ownership of your own thoughts and actions
    * Resolve disagreements one-on-one
    * When we feel like blaming, we reflect on our part in a situation and grow -- we can only change ourselves
    
## Annual Tasks

* Assemble and deliver MOU report to partner agencies for year
* Assemble and deliver president's report to the Annual General Meeting of members for year
* Renew RAC membership
* Foster and cultivate succession for source work
    
## Transition Tasks

* signing authority
* RAC membership
* LMERC mailing list
* MIERCT mailing list
* check the [Transition Guide](/guides/transition.md)


[koenig]: https://leadermorphosis.co/ep-49-peter-koenig-on-source-money-and-consciousness
[source]: https://medium.com/@tomnixon/where-does-the-purpose-of-a-company-come-from-71669e16d96f
[insights]: https://thejourney.reinventingorganizations.com/110.html
[purpose]: https://thejourney.reinventingorganizations.com/61.html
[finding]: https://thejourney.reinventingorganizations.com/65.html

[constellations]: http://www.nasconstellations.org/what-are-systemic-constellations.html
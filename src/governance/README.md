<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Governance](#governance)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Governance

<!-- DOCTOC SKIP -->

* [Core Elements: Governance Meeting](govelements.md)
* Example Agenda
* Example Meeting Record

Under law, the board meetings are the primary sessions for which you must keep records.  These are when directors manage liability and risk for the broader organization.  

At minimum, you keep records for governance sessions, special general meetings, and annual general meetings.  
# Planning an Agenda for a Governance Session

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Meeting Folder](#meeting-folder)
- [Format and Agenda](#format-and-agenda)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

Ideally, we collect the agenda titles and initial draft proposals in a proposals-motions worksheet before the session.  To help with preparing the session, we started using a meeting folder in our shared drives for each upcoming member and board session.  

## Meeting Folder

Details about the items used in the [governance session section](govelements.md):

* proposals-motions worksheet -- place to dynamically organize the agenda for the session 
* previous meeting's meeting summary and consent agenda -- bound together for approval
* reference material for the session, primarily PDF's -- for reference
* link to the board 'parking lot' record -- source of agenda items parked from previous sessions
* link to the board's action summary record -- source of agenda items for blocked or completed items


I wonder if using a tool like Loomio might make this easier.  

## Format and Agenda

Separating the format and the agenda make organizing the session convenient.  The format remains a constant structure with a set rhythm.  While the agenda varies to cover the items coming up on the 'radar' for the organization.  

With our month-long operating period, we aim to think about items **2 months** ahead.  

Use the [Governance Session Format Guide](/guides/govformat.md) for the session structure.  

To set the agenda, you can use one of four methods:

1. Collect agenda items between sessions in the proposals-motions worksheet
2. Review the parking lot for agenda items that need attention
3. Invite attendees to ponder during the opening segment and volunteer items on each person's mind at that moment
4. Consult the parking lot together during the opening segment for items that need attention (or culling)

![Visual Example](../img/agenda-gov.jpg)


# Core Elements: Board Session Governance Meeting

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Core Elements](#core-elements)
- [Reference Guides](#reference-guides)
- [Tools and Aids](#tools-and-aids)
  - [Meeting Folder](#meeting-folder)
  - [Proposals-Motions Worksheet](#proposals-motions-worksheet)
  - [Vote First, Then Discuss](#vote-first-then-discuss)
  - [Parking Lot](#parking-lot)
  - [Action Record](#action-record)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

To help with preparing the session, we started using a meeting folder in our shared drives for each upcoming member and board session.  I wonder if using a tool like Loomio might make this easier.  

## Core Elements

1. Time to Rig and Strike the Meeting
2. Opening (Check-in Round, Meeting Concerns, Confirming Agenda)
3. Processing the Agenda
4. Closing (Parking Lot, Check-out Round) 

## Reference Guides

* [Format](/guides/govformat.md)
* [Meeting Principles](/guides/meeting-principles.md)
* [Code of Conduct](/guides/conduct.md)

## Tools and Aids

### Meeting Folder

Meeting folder contains items in one place to conveniently refer to those needed in the session:

* proposals-motions worksheet
* previous meeting's meeting summary and consent agenda -- bound together
* reference material for the session, primarily PDF's
* link to the board 'parking lot' record (items that we parked from previous sessions)
* link to the board's action summary record

### Proposals-Motions Worksheet

Each session has a set amount of time we can dedicate to tackling agenda items.  Rather than a topic-only agenda, the session starts and ends with ways for people to connect and reflect together, which deserves the time allotted.  

Using a simple spreadsheet, allows us to slot the agenda items into the "process agenda" time box effectively.  We can either capture items in the moment or collect our thoughts beforehand and set priorities on the fly as we start the session.  Sorting uses the 'automatic filters' function.  

The worksheet captures key information about each **agenda item**:  

* Two-word description -- just to capture the idea for sorting and scheduling
* Initial Proposal wording -- needed for the initial assessing or level-setting vote before dialogue begins (can also use an "inquisitive how" question)
* Amended Proposal wording -- derived during session dialogue used for final agreement
* Final Motion -- notes on official binary vote for records

### Vote First, Then Discuss

Make sure each agenda item starts with a question to vote on before any dialogue begins.  Voting first helps you spotlight concerns faster.  Vote using a *fist to five* scale or a *probability spectrum*.  (Refer to [Meeting Participation](/guides/meetings.md).)

Examples:  

* **'Inquisitive How' Question**:  How safe is it to allow members to sign out equipment?

* **Proposal**:  I propose that we set up a way to make it seamless for members to use our online systems so that more members can connect with each other and with volunteering opportunities.  

### Parking Lot

To remember items to cover, capture items parked in the proposals-motions worksheet in the parking lot.  

Parking lot contains information about each issue that the board noticed yet felt comfortable setting aside for later.  Use the 'autofilter function' to sort the items each time you review the priority rank or add to the list.  

* Rank -- for chair to use for setting priority of items
* Item Name -- as noted in the worksheet
* Conceived (date) -- to set the age of the issue
* Board Exclusive -- to note which to reserve for board and which to raise at members session
* Processed (date) -- to mark off "done" items
* Drafted Proposal -- to record the latest draft of proposal describing the item

The chair tends the parking lot and treats it as a "product backlog".  Every item must have its _own_ priority rank, all different.  

### Action Record

Pending
# Keeping Records for a Governance Session

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


More soon

During my term, we reorganized our governance meeting format and have been experimenting with how to record that within the legal requirements and our traditional format.  

Refer to [Meeting Records Guide](/guides/meeting-records.md) for detailed information.  
# Guides

<!-- START doctoc -->


<!-- END doctoc -->

* [Contributing](contributing.md)
* [Meeting Participation](meetings.md)
* [Meeting Principles](meeting-principles.md)
* [Meeting Records](meeting-records.md)
* [Meeting Preparation](meeting-prep.md)
* [Team Huddles](teamhuddles.md)
* [Ladder of Leadership](ladder.md)
* [Operations and Intelligence Forum Format](oifformat.md)
* [Governance Session Format](govformat.md)
* [Director Role](directors.md)
* [Annual Transition](transition.md)
* [Practising Brevity](brevity.md)
* [Key Reference Books](core-library.md)

# Practising Brevity Means Structuring and Editing Your Message Carefully

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Use the BRIEF Map method to structure what we share to reach people who are overwhelmed with information](#use-the-brief-map-method-to-structure-what-we-share-to-reach-people-who-are-overwhelmed-with-information)
- [Start with a _point_](#start-with-a-_point_)
- [Arrange your thoughts into 5 supporting parts:  Beginning, Relevance, Information, End, Follow-up](#arrange-your-thoughts-into-5-supporting-parts--beginning-relevance-information-end-follow-up)
- [Edit down your message](#edit-down-your-message)
- [VECTOR must expect to receive structured messages from its President](#vector-must-expect-to-receive-structured-messages-from-its-president)
- [Follow-up](#follow-up)
- [Details:  Interview, Book Links, Template](#details--interview-book-links-template)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

McCormack:

> Professionals mistakenly abandon outlines, but a BRIEF Map is a new visual outlining tool that prepares you to be succinct.  

## Use the BRIEF Map method to structure what we share to reach people who are overwhelmed with information

* Shows your point early and gives more time for dialogue while together
* Preparing your message for people to receive improves your chance of being understood
* Understand how to deliver your message to different levels of depth to serve your audience's immediate need and interest
* Structuring your message visually helps convey it

## Start with a _point_

* Good points make sense when you put "I believe that" or "I propose that" at the front
* Only one point at a time ("...and...") suggests you're stacking points
* Including the response to "so what?" can help your point
* Consider mentioning the greatest impact or highest value within your point
* Use performance adjectives and drop all other adjectives


## Arrange your thoughts into 5 supporting parts:  Beginning, Relevance, Information, End, Follow-up

Supporting Element | Description | Notes
-------------------|-------------|---------------------------------------------
Beginning      | Background and context       | What is your beginning statement?  e.g.  "I have an update to our last question."
Reason         | Relevance                    | Why is it urgent and relevant to raise this item at this time?
Information    | Essential details            | What 3 key elements form the core of this item?  
End            | Conclusion or call to action | What concludes your item?  e.g. "I intend to update the website with those changes this week."  
Follow-up      | Anticipate additional detail | What questions do you think that your recipients will ask?  

## Edit down your message

* plan for 2 cycles of editing before sending your message
* aim for essentials rather than completeness
* levels of detail (imagine how much time you want your recipient to invest)
    * 10-second version - headline point
    * 2-minute version - the visible subheadings
    * 10-minute version - the bullets
    * detailed version - the links
* good communicators often spend hours preparing for an important 5-minute presentation
* arrange your message elements visually using styling
* consider using a single diagram

## VECTOR must expect to receive structured messages from its President

* Structuring materials circulated for board and member meetings makes ideas easier for people to read in advance
* Directors and other volunteers in leadership roles can practise the method

## Follow-up

* Structuring messages gives us chance to stop using time together to re-share information circulated to participants
* Listening to McCormack talk to McKay (below) gives quick background

---

## Details:  Interview, Book Links, Template

* 30 minute interview talking about the key points:  [Joe McCormack with Brett McKay](https://www.artofmanliness.com/articles/how-to-be-a-better-communicator/)

**References:**

* [Joel Schwartzberg](https://www.joelschwartzberg.net/). [_Get to the Point_](https://vpl.bibliocommons.com/item/show/5533482038).  Exemplary **short** book.  [Personal Notes](https://jesseneri.ca/sources/165-get-to-the-point_)
* [Joseph McCormack](https://thebrieflab.com/). [_Brief_](https://vpl.bibliocommons.com/item/show/3639282038)
* Joseph McCormack. [_Noise_](https://vpl.bibliocommons.com/item/show/7101679038).  Covers core message from _Brief_ in 3 pages alongside explaining how to communicate to people who are "under water" in information and reading.  
* [Podcast: "Just Saying"](https://podcast.thebrieflab.com/) (15 to 20 minute clips)

**Format Suggestion**

To summarize and report background for a board item.  Let's use the format below.  

> ### Headline (Heading 1 format)
>
> Link to detailed version
>
> #### First component or Beginning and Reason (Heading 2 format)
>
> * note for first component
> * note for first component
>
> #### Second component, Information group, or first Information element
>
> * key element 1
> * key element 2
> * key element 3
> 
> #### Third Component or second Information element
>
> * note
> * note
> 
> #### Fourth Component or third Information element
>
> * note
> * note
> * note
>
> #### End element
>
> #### Follow-up element
> 
> * answer anticipated question
> * answer anticipated question
> * ask question
>

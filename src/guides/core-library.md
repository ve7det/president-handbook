# Four useful books

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Leadership is Language - L. David Marquet](#leadership-is-language---l-david-marquet)
- [A World without Email - Cal Newport](#a-world-without-email---cal-newport)
- [One Mission - Chris Fussell](#one-mission---chris-fussell)
- [A Paradise Build in Hell - Rebecca Solnit](#a-paradise-build-in-hell---rebecca-solnit)
- [Follow-up](#follow-up)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Leadership is Language - L. David Marquet

**The Hidden Power of What You Say and What You Don't**

Outline: (in progress, need to review the book again)

* Foreword
* Introduction
    * Turning the ship around
    * Leadership is language
    * Spreading the word about language
    * A new leadership playbook
    * Balancing deliberation and action
    * How did we get here?
* Chapter 1: Losing _El Faro_
    * Tuesday
    * Wednesday
    * The language of vulnerability
    * Thursday
    * The danger of old thinking in new situations
    * Share of voice
* Chapter 2: The New Playbook
    * Anchored to an outdated playbook
    * Embracing vs. reducing variability
    * Redwork and Bluework
    * The end of blueworkers and redworkers
    * Can I get some thinking?
    * Red-blue approaches
    * The rhythm of redwork-bluework
    * The thinking animal
    * The brain's response to stress and motivation
    * Adapting for the future
* Chapter 3: Exiting Redwork: Control the Clock
    * Who's to blame?
    * How to control the clock
        * ...
* Chapter 4: Into the Bluework: Collaborate
    * How to collaborate
        * ...
    * Establish the right hypothesis
* Chapter 5: Leaving Bluework Behind: Commit
    * Consequences of compliance
    * How to commit
        * ...
    * What do committing statements sound like?
    * Coulda, Woulda, Shoulda
    * In for a penny, in for a pound: escalation of commitment
    * Separate the decision-maker from the decision-evaluator
* Chapter 6: The End of Redwork: Complete
    * Completion is critical
    * Keep on rollin'
    * How to complete
        * ...
* Chapter 7: Completing the Cycle: Improve
    * When to improve
    * Aim for discontinuous improvement
    * All together now
    * The "be good" self vs. the "get better" self
    * Our three emotional needs
        * ...
    * It's my life
    * Overcoming learned helplessness
    * How to improve
        * ...
    * Using the timeline as a tool
    * Thinking as a team
    * Study of pilots shows it's all true
    * A lesson from agile management
* Chapter 8: The Enabling Play: Connect
    * Hierarchy first
    * How to connect -- underlying requirement for all plays
        * ...
    * Patient Elliot
    * Gambling in Iowa
    * Humans and Hierarchy
* Chapter 9: Applying the Redwork-Bluework Principles in Workplace Situations
    * A quick recap of our new playbook
    * Interference or controlling the clock: when to exit redwork
    * Enough talk already: managing a change initiative
    * We can't stop, we're going too fast: pausing to improve
    * All quiet on the idea front: using connect to make it safe
    * I'm starting to think it's you not me: more effective collaboration
    * Getting commitment instead of compliance
    * Put your own mask on before helping others
    * Avoiding the pitfalls of the old playbook
    * Using red-blue thinking to control situations better
    * Making it safe for my boss
    * Job descriptions, hiring, and evaluations
* Chapter 10: The Red-Blue Operating System
    * The perils of goal setting
    * Strict goals + steep hierarchies = unethical behaviour
    * The red-blue annual cycle
    * Just sell one engine
    * Revisiting agile
    * Bluework at the tactical level: deliberate action
    * Self-regulation
    * Lifelong learning
* Chapter 11: Saving _El Faro_
    * Monday
    * Wednesday
    * Friday
* Acknowledgments
* Further Reading
* Glossary
* Notes
* Index

## A World without Email - Cal Newport

**Reimagining Work In An Age Of Communication Overload**

Outline:

* Introduction: The Hyperactive Hive Mind
* Part 1: The Case Against Email
    * Email Reduces Productivity
    * Email Makes Us Miserable
    * Email Has a Mind of Its Own
* Part 2: Principles for a World Without Email
    * The Attention Capital Principle
    * The Process Principle
    * The Protocol Principle
    * The Specialization Principle
* Conclusion: The Twenty-First-Century Moonshot
* Acknowledgements
* Notes
* Index
    

## One Mission - Chris Fussell

**How Leaders Build a Team of Teams**

Outline:

* Foreword by General Stanley McChrystal
* Introduction
* One Mission
* The Hybrid Model
    * Case Study: Intuit
* Interconnection
    * Case Study: Oklahoma Office of Management and Enterprise Services
* Operating Rhythm
    * Case Study: Under Armour
* Decision Space
    * Case Study: Medstar
* Liaisons
    * Eastil Secured
* Conclusion
* Appendix: Chief of Staff
* Acknowledgements
* Notes
* Index


## A Paradise Build in Hell - Rebecca Solnit

**The Extraordinary Communities That Arise in Disaster**

Outline:

* Preface to the Revised Edition
* Prelude:  Falling Together
* I. A Millenial Good Fellowship: The San Francisco Earthquake
    * The Mizpah Cafe
    * Pauline Jacobson's Joy
    * General Funston's Fear
    * William James's Moral Equivalents
    * Dorothy Day's Other Loves
* II. Halifax to Hollywood: The Great Debate
    * A Tale of Two Princes: The Halifax Explosion and After
    * From the Blitz and the Bomb to Vietnam
    * Hobbes in Hollywood, or the Few Versus the Many
* III. Carnival and Revolution: Mexico City's Earthquake
    * Power from Below
    * Losing the Mandate of Heaven
    * Standing on Top of Golden Hours
* IV. The City Transfigured: New York in Grief and Glory
    * Mutual Aid in the Marketplace
    * The Need to Help
    * Nine Hundred and Eleven Questions
* V. New Orleans: Common Grounds and Killers
    * What Difference Would It Make?
    * Murderers
    * Love and Lifeboats
    * Beloved Community
* Epilogue: The Doorway in the Ruins
* Gratitude
* Notes
* Index


## Follow-up

* [More on _Leadership is Language_](https://jesseneri.ca/sources/175-leadership-is-language)
* [More on _One Mission_](https://jesseneri.ca/sources/50-one-mission)
* Remember also: (via our [brevity principles](guides/brevity.md))
    * Noise - Joe McCormack
    * Brief - Joe McCormack


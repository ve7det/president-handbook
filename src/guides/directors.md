# Director Role in VECTOR

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Duties](#duties)
- [Key Responsibilities:](#key-responsibilities)
- [Enabling Responsibilities](#enabling-responsibilities)
- [How I served on the VECTOR board](#how-i-served-on-the-vector-board)
- [Specific Tasks I did as a Director](#specific-tasks-i-did-as-a-director)
- [Further Information](#further-information)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Duties

* Care about success of VECTOR activities and operations

* Loyal to VECTOR before my personal and professional interests

* Obey applicable federal, provincial and local laws as well as VECTOR bylaws to guard VECTOR's mission

## Key Responsibilities:

* Establish VECTOR identity through effective strategic and adaptive planning and advocating for VECTOR's mission and purposes

* Ensure resources through building a competent board, enhancing VECTOR's public standing and ensuring adequate financial resources

* Oversee VECTOR activities through supporting program volunteers, protecting assets, overseeing spending, protecting integrity, monitoring programs, strengthening services

## Enabling Responsibilities

* Strengthen programs and services so that VECTOR can achieve its mission and purposes

* Create space for VECTOR volunteers to implement programs that deliver VECTOR's mission and purposes

* Protect assets and provide proper financial oversight using annual budgets and financial controls; faithfully read and understand VECTOR financial statements

* Serve as active ambassadors for VECTOR (a link between members, stakeholders, constituents and clients) to educate influencers and the community (local, provincial and federal) about the importance of VECTOR's work and to enhance VECTOR's community relationships

* Build a collaborative and competent board through connection, continuous improvement, learning and mentoring; and assess our own performance governing VECTOR

* Focus on listening to ensure that everyone has a chance to share opinions and understand each other

* Ask "naive" questions to ensure current and proposed projects serve VECTOR's mission and are setting VECTOR up for success

* Serve as a court of appeal for internal issues within the conflict mechanism

* Passionately enjoy VECTOR's work and are interested in learning more about VECTOR's mession and the community served

* Help identify personal connections that can benefit VECTOR

* Engage connections, networks and resources to develop collective action to fully achieve VECTOR's mission

* Prepare for, attend, and conscientiously engage in board meetings

* Support active committees or working teams, attending regular huddles and helping each one with internal processes such as project plans, expense claims, and reimbursements (minimum 1, maximum 3)

## How I served on the VECTOR board

2014  
- helped manage membership

2015  
- managed membership induction and identification cards

2016  
- supported planning and logistics committee teams to establish training grid and begin efforts to update plans and manuals

2017  
- supported membership committee team with identification cards

2018  
- created 6 Operator School skill sessions and supported members to build skills
- supported games and exercises committee team to create field and community centre exercises
- supported "MRO" Jobs committee team to create framework to improve work sessions
- began learning how to strengthen the VECTOR board based on successes of other non-profit boards
- started reconnecting VECTOR to ESS Go To Community Centres

2019  
- continued supporting learning sessions, games and exercises
- supported start of Kerrisdale support team and Marpole support team
- supporting coaches to create and start HF hands-on sessions
- supported the work of the Mark 2 (Next Generation) Grab and Go Kit
- supported steering committee for Kerrisdale Earthquake and Emergency Preparedness community group

## Specific Tasks I did as a Director

* Host operator skill sessions (learning opportunities)

* Facilitate early huddles for Team Grab and Go

* Attend games and exercises huddles

* Liaise with community centres to arrange on-site access for radio exercises

* Arrange rooms with community centres and VPL branches

* Arrange rooms through VEMA and ECOMM

* Approve and relay expense claims from active volunteers to Treasurer

* Relay reimbursement cheques to active volunteers from Treasurer

* Assemble registration batches for EMBC Public Safety Lifeline Volunteers

* Assemble batches for City security badges

* Prepare and mail member identification cards

* Support coaches delivering "Grab and Go Kit Hands-On" tutorial sessions

* Support coaches delivering "Data Get Started and Play" sessions and packet nights


## Further Information

* [Resource Library - PDF's][1]

[1]:https://drive.google.com/open?id=1cy8YUbCT_aYh3HPn396LwltqHSXARuxC

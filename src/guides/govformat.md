# Board Meeting Format

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

<!-- DOCTOC SKIP -->

2 hours active VECTOR time.  Let's use this format for 2020 sessions ([governance meeting format][1]).  Let's also apply the **[spirit and principles][2]** from [Robert's Rules of Order New Revision][3] over the letter of it.  Details in the [governance section](/governance/govelements.md).  

---

**18:30 - T-30: Rig Zoom - 00:30**  

* Prepare online space and teleconference gear

---

**18:45 - T-15: Unofficial Start - 00:15**

* Open virtual space for online sessions

---

**19:00 - T: OPENING - 00:30**  

* **Check-in Round** 

    * One at a time. One minute each. Call out distractions, share what you need to so you can feel present. **No discussion.**  For team to clear minds
    * Leaving early? What expected interruptions? What are you looking forward to? What's on your mind?  

* **Admin Concerns** - Address any logistical concerns for the meeting itself (including distractions noticed during check-in).  For team to clear minds

* **Check Dashboard** - See state of VECTOR, Append to meeting record

* **Build Agenda**

    * Note [items on your mind][4] (2 words each) and initialize an idea parking lot  
    * Pick priority and 4 focus items to cover  
    * Option to assemble agenda list and initial proposal wording in month's proposals worksheet

---

**19:30: - T+30: PROCESS AGENDA - 00:27** (Records required)  

* **Approve Consent Agenda** (includes fiscal updates) - Move anything you have questions about to the main agenda, consent to rest.  Note changes in meeting record and append to meeting record  

* Process priority item and up to one focus item (present, clarify, react, amend, object, integrate, iterate)

**19:57 - T+57: Net Break - 00:05**

* Make announcements on our weekly net  
* Resolve biological demands  

**20:02 - T+62: Process Agenda Continues - 00:48** (Records required)

* Process remaining focus items and any other items that fit time box  

---

**20:50: - T+110 : CLOSING - 00:10** (Records required)

* **Review Parking Lot** - Choose together path forward for each parked item:  **hold** for next month, resolve over email, or **discard**  

* **Share reflections**  - One at a time.  Capture "I like" and "I wish" items to improve the next session.  **No discussion.**  

---

**21:00 - T+120: Strike Zoom - 00:30**

---

**21:30 - T+150: Unofficial End and Lock Up**

* Close virtual space for online sessions
* Lock up meeting room at in-person sessions

---

![Visual example](../img/agenda-gov.jpg)

---

[1]:https://www.holacracy.org/governance-meetings

[2]:https://www.thevantagepoint.ca/blog/our-vantage-point-episode-8-whats-deal-roberts-rules-order

[3]:http://www.elimina.com/insights/articles-rules.html

[4]:https://nericanada.sandcats.io/shared/6VYyNNmvw6Oa1SY0meT_chpue86Fsjr_XiC5C2ybZqz

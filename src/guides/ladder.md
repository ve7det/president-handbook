# Understand Ladder of Leadership

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

More soon

Learn more through
* [short videos](https://intentbasedleadership.com/category/leadership-nudge/tools-for-leaders/ladder-of-leadership/) or the post on [six ideals at VECTOR](https://vectorradio.ca/posts/2020/forward/#leadership-is-language)
* [interview](https://thefutureorganization.com/leadership-is-language/)


When reading L. David Marquet's _Leadership is Language_, I learned that being the decision evaluator or coordination point is the true value of a leader.  

> ...if I can only keep my mouth shut for a few extra seconds, ask the kinds of questions that encourage people to share their thoughts, and actually pay attention to what others are saying, their ideas, points of view, and suggested actions are often as good as -- often even better (!) -- than what I'd had in mind.  


Wendy Sullivan, Judy Rees, _Clean Language: Revealing Metaphors and Opening Minds_. 2008.  


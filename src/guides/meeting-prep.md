# Preparing for Meetings

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Key Ideas](#key-ideas)
- [Tools](#tools)
- [Reference Resources](#reference-resources)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Key Ideas

* objective in 10 words or less
* list items that define minimum success from meeting
* share objective and success criteria with attendees in advance
* mechanisms for preparing agenda and notes for agendas
* using a 'parking lot'
* approaching guest speakers

## Tools

* Timing agenda (visual examples in figures below)
* agenda worksheet
* parking lot
* topics list (suggestion record)
* facilitators cues (figure below), or technical facilitation tool

## Reference Resources

Visual Examples: 

![Governance Timing Agenda](../img/agenda-gov.jpg)

![O&I Forum Agenda](../img/agenda-oi.jpg)

![Facilitator Cues](../img/cues.jpg)

# Meeting Principles

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Foster Intentional Dialogue Balancing Candor and Curiosity](#foster-intentional-dialogue-balancing-candor-and-curiosity)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

From _Robert's Rules of Order New Revision_ via [Eli Mina][3] on [Vantage Point Podcast][2] and [_Conversational Capacity_][4]

---

**Order**
: Give space to only 1 speaker at any time, no interrupting

**Focus**
: Focus on mandate and issue at hand

**Efficiency**
: Give agenda items time deserved

: Speak clearly and briefly

**Equality**
: Listen to everyone else at least once before you speak again

**Decorum**
: Focus on issue not person sharing their view 

**Safe Environment**
: Help keep an emotionally safe environment for effective sharing of ideas

---

## Foster Intentional Dialogue Balancing Candor and Curiosity

**Share with candor**

* We state our clear position concisely without using minimizing phrases to distract from our point or dilute our position

* We explain our thinking that brought us to our position

**Explore with curiosity**

* We test our own view for validity

* We inquire about other people's views on an issue



---

[1]:https://www.holacracy.org/governance-meetings

[2]:https://www.thevantagepoint.ca/blog/our-vantage-point-episode-8-whats-deal-roberts-rules-order

[3]:http://www.elimina.com/insights/articles-rules.html

[4]:https://jesseneri.ca/source/161-conversational-capacity-the-secret-to-building-successful-teams-that-perform-when-the-pressure-is-on
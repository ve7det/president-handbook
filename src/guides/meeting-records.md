# Understand Meeting Records

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Key Ideas](#key-ideas)
- [Reference Resources](#reference-resources)
  - [Insights](#insights)
  - [British Columbia](#british-columbia)
  - [Canada-wide or Other Provinces](#canada-wide-or-other-provinces)
  - [Robert's Rules](#roberts-rules)
  - [Possible video courses in New Zealand](#possible-video-courses-in-new-zealand)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Key Ideas

* Meeting records form an important part of an organization's records.  
* Report the session logistics, summary of dialogues, decisions, and actions noted
* Balance keeping enough information to correctly show deliberations and decisions with risk of unintended use in audit or litigation
* Summarize dialogues clearly, accurately and objectively
* Show enough detail to demonstrate thoughtful deliberation and dialogue
* Show how much challenge and review people gave important matters, including engagement in setting strategy and risk tolerance
* Capture objections and abstentions for decision records
* List people who joined late or left early (for decision records)
* "Minutes should **not** reflect all questions asked and responses given, nor as a general rule should they identify which director asked a particular question" (Nathan)
* May use Robert's old business and new business agenda or a functional agenda


## Reference Resources

### Insights

* [Miller Thomson Lawyers, "Do we have to put all that in the minutes?"](https://www.millerthomson.com/en/publications/communiques-and-updates/social-impact-newsletter/may-2011-social-impact-newsletter-formerly-the/do-we-have-to-put-all-that-in-the-minutes/)

* [Hartley Nathan, "Minutes and Notes of Board Metings"](https://www.mindengross.com/docs/publications/docs1--1895332-v1-corporate_requirements_to_keep_minutes_-)

* [Ryan Prendergast, "Keeping Minutes: Getting It Down Right"](http://carters.ca/pub/seminar/charity/2017/Handout2017-agenda.pdf) (turn to pages 33 to 36)

* [Torkin Manes Lawyers, "Pre-Meeting Fundamentals"](www.torkinmanes.com/docs/default-source/publications/newsletters/march-2016---pre-meeting-fundamentals-for-ontario-not-for-profit-corporationsCFFCFCE7BAC7)

* [Don Kramer, "Preparing Minutes of Board Meetings Is Usually More Art Than Science"](https://www.nonprofitissues.com/ready-reference/preparing-minutes-board-meetings-usually-more-art-science)

* [Governing Good, "Guide to Great Board Minutes"](http://www.governinggood.ca/wp-content/uploads/2013/07/A-Guide-To-Great-Board-Minutes.pdf), [Dalhousie Collection](http://www.governinggood.ca/resources/governance-guides-dalhousie-collection/)

* [The Balance, "Board Meeting Minutes Mistakes"](https://www.thebalancesmb.com/board-meeting-minutes-mistakes-4136593)

* [Minden Gross Lawyers, "Corporate Requirements to Keep Minutes"](https://www.mindengross.com/docs/publications/docs1--1895332-v1-corporate_requirements_to_keep_minutes_-)

* [Better Boards, "Meeting Minutes, An Essential Guide for Directors"](https://betterboards.net/governance/meeting-minutes-quick-guide/)

### British Columbia

* [Legal Self Assessment Tool for Non-profits in BC](https://lawfornonprofits.ca/)

* [Clicklaw, "Record Keeping for Societies Act"](https://wiki.clicklaw.bc.ca/index.php?title=Record-Keeping_(Societies_Act_FAQs))

### Canada-wide or Other Provinces

* [Denise Edwards, "Record Keeping for Non-Profit Organizations"](http://www.omafra.gov.on.ca/english/nfporgs/08-059.htm#meetingminutes)

* [Nonprofit Law Ontario, "Records and Minutes"](https://nonprofitlaw.cleo.on.ca/current-law/running-a-nonprofit/records-and-minutes/)

* [Nonprofit Law Ontario, "Member and Board Meetings"](https://nonprofitlaw.cleo.on.ca/current-law/running-a-nonprofit/member-and-board-meetings/)

* [Don Kramer, "Are Meeting Minutes Legal Documents?"](https://www.nonprofitissues.com/to-the-point/are-meeting-minutes-legal-document)

* [Governing Good, "Daylighting Board Minutes"](http://www.governinggood.ca/daylighting-board-minutes/)


### Robert's Rules

* [C. Alan Jennings, "Meeting Minutes According to Robert's Rules"](https://www.dummies.com/careers/business-skills/meeting-minutes-according-to-roberts-rules/)

* [C. Alan Jennings, "Robert's Rules for Dummies Cheat Sheet"](https://www.dummies.com/careers/business-skills/roberts-rules-for-dummies-cheat-sheet/)

### Possible video courses in New Zealand

* [Not For Profit Resource, "Meetings and Minutes"](https://www.not-for-profit.org.nz/meetings-and-minutes/)


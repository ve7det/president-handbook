# Meeting Participation

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Fist to Five Poll](#fist-to-five-poll)
- [Probability Spectrum](#probability-spectrum)
- [Feelings Check-in Tool](#feelings-check-in-tool)
- [Hand Signals](#hand-signals)
- [Meeting Materials](#meeting-materials)
- [Binary Vote](#binary-vote)
- [Always Applicable](#always-applicable)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Fist to Five Poll

Use this to understand how people feel before you start.  You can use this system in two ways:  

1. Asking "how" people feel about something, such as "how risky is X?"
2. Proposing a course of action, such as "I propose that we set up a team to host nightly nets during the 2020 pandemic so members can use radios to converse."

At the start of a discussion, a fist of five vote sets the discussion level.  This polling style shows who supports the idea and helps find who sees something you missed.  

_Number_ | _Alternative_ | _Meaning_
---|---|---
0 | fist | No.  
1 | 1 finger | I hold some reservations about this idea.  
2 | 2 fingers | I hold only one concern about this idea.  
3 | 3 fingers | Sure, let's give it a shot.  
4 | 4 fingers | I love this idea.  
5 | 5 fingers | Could I take over or help out?  


[Lucid Meetings](https://www.lucidmeetings.com/glossary/fist-five)  
[Meeting Guru](https://debiwilcox.com/blog/2020/1/6/making-decisions-with-fist-to-five-voting)  
[L. David Marquet, Captain US Navy Retired](https://www.davidmarquet.com/2017/03/01/fist-to-five/)  

## Probability Spectrum

Where needed, use the probability spectrum to also gauge how people feel.  

> How strongly do you believe we should practise more often?

Options:  1%, 5%, 20%, 50%, 80%, 95%, 99%.  

## Feelings Check-in Tool

A way to signal your comfort level to each other.  Rebootio uses a simple three-stage traffic-light gauge:

[![](../guides/20191111-I5-1-rebootio-checkintool-1*KX0YTxlzFeoM17wyN8NYTg.jpg)](https://www.reboot.io/2017/09/07/murmuration/)

> **Green** means you feel safe, copasetic, or perhaps are in flow. Your[sic] able to have eye-contact, creativity, play, humor. In a sense, all systems are go.
>
> **Yellow** is reactionary, meaning that the fight or flight impulse is present, as is perhaps some defensiveness.
>
> **Red** means your rational brain is offline, nervous system is shutting down such that you may or may not be present at all, or there may be a loss of trust.

Also, learn about [non-violent communication principles](https://firstround.com/review/power-up-your-team-with-nonviolent-communication-principles/)


Can't find the source for the image, though.  

## Hand Signals

You may consider using hand signals to silently express how you feel about an idea.  Alternatives come from the consensus decision-making methods used by:

* [Meeting Hand Signals](http://www.lovelifepractice.com/practice/using-hand-signals-for-better-meetings/)
* [Edutopia](https://www.edutopia.org/video/using-hand-signals-more-equitable-discussions)
* [Seeds for Change Cooperative Resources](https://www.seedsforchange.org.uk/resources)
* [Working Den](https://www.workingden.com/virtual-meeting-hand-signals/)
* [Nasco Cooperative Resources](https://www.nasco.coop/resources)
* [History](https://en.wikipedia.org/wiki/Occupy_movement_hand_signals#Origins)


## Meeting Materials

* in drive...

## Binary Vote

When a team needs to register a decision as a motion, we use a binary vote to count out who supports the decision.  

## Always Applicable

* [Meeting Principles](meeting-principles.md)
* [Code of Conduct](conduct.md)

# O&I Forum Format

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

<!-- DOCTOC SKIP -->

([Operations][1] [and][2] [Intelligence][3] [Forum][4]) 2 hours active VECTOR time.  30 minutes at each end for rigging and striking Zoom.   Prepare the session using our technical facilitation tool (based on [the one at theHum](https://www.thehum.org/post/technical-facilitation-in-online-meetings) or [others](https://www.qwant.com/?q=facilitation+tool&t=web).  

---

**18:00 - T-30: Rig Zoom - 00:30**  

* Prepare online space and teleconference gear
* Check signal between attendees

---

**18:15 - T-15: Unofficial Start - 00:15**

* Open virtual space for online sessions

---

**18:30 - T: Opening Aligning Narrative - 00:01**

* Call session to order, acknowledge First Nations, and describe our common purpose

---

**18:31 - T+1: Check-in Round - 00:09**  

* Give attendees time to say a sentence about what's currently on their mind or heart

---

**18:40 - T+10: Topical Show - 00:20**  

* Help members learn something new so that everyone gets closer to a world where amateur radio teams support Vancouver's parts with VEMA and VPD to deliver messages whenever needed
    
---

**19:00 - T+30: Break - 00:05**  

---

**19:05 - T+35: Aligning Narrative - 00:01**

* Call session back to order highlighting current situation

---

**19:06 - T+36: Practical Exercise - 00:14**

* Give a member the chance to run a suitable skill drill to help everyone keep sharp

---

**19:20 - T+55: O&I Forum - 1:08**  

* Give active teams a time to coordinate
    * What did you notice since last time?
    * What do you need?

* Create, with the required **regularity**, conditions for the organic **interaction** of our **teams**, and reiterate an **aligning narrative** to the assembled organization.  A regular **reminder of purpose** and a chance to **reconnect with the larger tribe**.  

* Discuss conditions, emerging opportunities or threats, intended next steps, and why emerging observations matter to other teams in VECTOR

* Consider using a high level team task board as an artifact to prompt dialogues about
    * stuck efforts
    * milestones reached
    * open loops

* Consider using 'tactical meeting' format as a guide to get active teams coordinating

---

**20:15 - T+105: Check-out Round - 00:10**  

* Give attendees time to share, "I will...", "I like" and "I wish" items to share commitment and improve future sessions

---

**20:25 - T+115: Closing and Gratitude - 00:05**

* Reiterate the aligning narrative, just cause, and team gratitude

---

**20:30 - T+120: Strike Zoom - 00:30**  

* Put away Zoom equipment at in-person sessions

---

**21:00 - T+140: Unofficial End and Lock Up**

* Lock up meeting room at in-person sessions

* Close virtual space for online sessions

---

Example:
![O&I Forum](../img/agenda-oi.jpg)

---
[1]:https://youtu.be/e0of2wwq35w?t=2456

[2]:https://youtu.be/UtUZKd-6zio?t=1374

[3]:https://www.youtube.com/watch?v=BYeI5bUk52k

[4]:https://youtu.be/3VF3n7mY2f8?t=3462



# How to write a process

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Structure](#structure)
- [Considerations](#considerations)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

I recommend using a way to assemble procedure records that Jono Bacon describes in [_Art of Community_][aoc].  You can find it at our [library][aocvpl].  Refer to Chapter 4 (Simple is Sustainable).  

Writing down your processes makes remembering details, teaching others, and improving the method all easier.  Ideally, we could fit processes on a single page.  Two pages is also quite good.  

Naming the process helps people find it.  Consider including "how to" at the start of the file name and finishing it with the shortest verb phrase that describes what you're doing.  

## Structure

Four sections:

1. Objective - what do you wish to achieve?
2. Goals - what are your results?
3. Success criteria - how do you recognize you're succeeding?
4. Implementation - what are the steps and assumptions?
5. Owner - who takes care of the process?

Example:  [How to Get Reimbursed][example]

## Considerations

to do

[aoc]: https://www.jonobacon.com/books/artofcommunity/
[aocvpl]: https://vpl.bibliocommons.com/item/show/1944501038
[example]: "How to Get Reimbursed.pdf"
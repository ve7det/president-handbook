# Hosting a Team Huddle

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Purpose](#purpose)
- [Rough Pattern](#rough-pattern)
- [Wise Practices](#wise-practices)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Purpose

Active VECTOR teams hold team huddles to stay connected and get help with obstacles.  

## Rough Pattern

A good pattern for a team huddle matches a "tactical meeting":

1. **Check-in round.** Put rest of day behind and get present: each person describes what they were doing right before heading out to the huddle, in turn, as monologue only

2. **Build agenda.** Collect from members items to cover

3. **Process agenda.** Work through items to cover

4. **Check-out round.** Give each person time to reflect on the huddle each person completes three statements about the huddle, in turn, as monologue only:  "I will... by next time", "I liked...", "I wish..."

## Wise Practices

* Give room for casual connection: set unofficial start and end times, around 10 minutes before and after online sessions

* Start on time

* Share the roles:  rotate the work of facilitator, timekeeper, notetaker (scribe) each time

* Use a communal online scratchpad to capture notes unless you need a strict meeting summary

* Schedule session to last between 30 and 180 minutes only

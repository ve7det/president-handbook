# Board Transition

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Administrative Transition Elements](#administrative-transition-elements)
- [Financial Transition Elements](#financial-transition-elements)
- [Other Transition Elements](#other-transition-elements)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Administrative Transition Elements

* Export board member contact information from member data to include in annual Societies Act filing and for board roster in [MOU Report](moureport.md)
* Circulate board contact list to partner agencies
* Revise 'executive@vectorradio.ca' group list
* Revise board roster on website
* Update access control lists (for site access) to show changes in executive team
* Revise aliased e-mail addresses for officer roles

## Financial Transition Elements

* Revise signing authorities for change in officers
* Open accounts at organization's bank for signing officers

## Other Transition Elements

Pending


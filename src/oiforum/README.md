# O & I Forum

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Why do it?](#why-do-it)
- [What is it?](#what-is-it)
- [External Guides](#external-guides)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Why do it?

1. To reinforce shared purpose.
2. To reconnect with other members.
3. Discuss emerging conditions to steer between sessions.  

## What is it?

A common space where all members can come at regular intervals to interact with teams and other members.  

In this space, leaders must reiterate an aligning narrative to the assembled organization and give everyone a regular boost of purpose.  Reconnecting with the larger "tribe" also helps reinforce spirit.  

Time to discuss conditions, emerging opportunities or threats, intended next steps, and why emerging observations matter to other teams in VECTOR.  

## External Guides

* Chris Fussell. _One Mission_.
* L. David Marquet. _Leadership is Language_.
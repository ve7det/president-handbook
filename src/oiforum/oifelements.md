# Core Elements:  Members Meeting Operations and Intelligence Forum

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


<!-- DOCTOC SKIP -->

1. Time to Rig and Strike the Meeting
2. Aligning Narrative
3. Check-in Round
4. Topical Show Segment
5. Forum Segment

    * Practical Exercise
    * Active Forum

6. Check-out Round


Reference Guides

* [Format](/guides/oifformat.md)
* [Meeting Principles](/guides/meeting-principles.md)
* [Code of Conduct](/guides/conduct.md)


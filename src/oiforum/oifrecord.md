# Keeping Records for an O & I Forum

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
<!-- END doctoc generated TOC please keep comment here to allow auto update -->

More soon

Operations and Intelligence Forum sessions behave differently than a traditional meeting.  

Refer to [Meeting Records Guide](/guides/meeting-records.md) for detailed information.  
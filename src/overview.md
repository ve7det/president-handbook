# Overview

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Essential Role](#essential-role)
- [Functions](#functions)
- [Recommended Definition of VECTOR](#recommended-definition-of-vector)
  - [References](#references)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Essential Role

As president in VECTOR, you fill one of the 4 official roles required under law in British Columbia for non-profit societies.  VECTOR operates as a charity non-profit society registered under the [BC Societies Act][act] and Societies Regulations.  

The Act and Regulations protect the public and VECTOR members while also outlining the responsibilities of organizations and members.  

VECTOR's status as a charity and its status as a non-profit form essential parts of where it serves in our local community.  

[act]: https://www.bclaws.ca/civix/content/complete/statreg/1527898742/15018/?xsl=/templates/browse.xsl

## Functions

Members rely on the president for several core functions.  All VECTOR activities depend on how successfully a president performs the following functions:

* Listen to VECTOR, the organization, and its just cause
* Chair and facilitator of governance meetings for the Board of Directors
* Facilitator of Monthly Member Meetings, including the Operations and Intelligence Forum
* One of the Account signatories for banking
* Initial liaison to regional emergency communications coordination committees
* Initial liaison to Vancouver Police Department
* Initial liaison to Vancouver Emergency Management Agency
* Initial liaison to Radio Amateurs of Canada
* Champion for VECTOR in the community -- a role all board members share

We can arrange these functions into four categories:

1. [Executive](executive/README.md)
    * steward for VECTOR's just cause
    * account signatory
    * initial liaison to external agencies
2. [O&I Forum](oiforum/README.md)
    * chair for monthly member meetings, including the operations and intelligence forum
3. [Governance](governance/README.md)
    * chair for governance meetings (Board of Directors)
4. [Community](community/README.md)
    * champion VECTOR in the community
    * connect with community groups for service and partnership


## Recommended Definition of VECTOR

**VECTOR**
: Specializes in establishing and maintaining emergency communications during major emergencies and civic events.  VECTOR members are skilled in making sure messages get delivered clearly and adept in using many means of communication.  VECTOR operates primarily using amateur radio technologies at disaster sites when other communication systems have failed.  


### References

**VECTOR's Just Cause**
: Amateur radio teams supporting every part of Vancouver with our partners to deliver messages whenever needed (especially when other systems fail or are inappropriate).  

**VECTOR's Vision**
: Bringing together the power of ordinary citizens and amateur radio to enhance community resiliency for times of emergency and disaster.  

**VECTOR's Mission**
: VECTOR exists as the organizing vehicle for members of the amateur radio service to build auxiliary emergency communications capability to enhance community resiliency in the City of Vancouver.

: Our mission is communications preparedness and the most important element of readiness is people, not technology. Through recruitment, training and exercise we develop our diverse volunteer team to plan and prepare for those times when we are called upon to serve.


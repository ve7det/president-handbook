# Policies

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

- [Finance Policy](#finance-policy)
- [Community Code of Conduct](#community-code-of-conduct)
- [Volunteer Code of Conduct](#volunteer-code-of-conduct)
- [EMBC Volunteer Code of Conduct](#embc-volunteer-code-of-conduct)
- [Confidentiality Agreement](#confidentiality-agreement)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Finance Policy

Our rules for making sure we use our limited money well is in our organization files in [our Google Suite][fin].  

Or here:  [Finance Policy](LATEST VECTOR Finance Policy - updated 20191217.pdf)

[fin]: (https://drive.google.com/file/d/0BzjoISDBI_TUWFFRRHFFYVlmazMtNTRKN1ZQNGxabHFSRkNB/view)

## Community Code of Conduct

Our rules for making safer spaces for each other.  Please refer to the [live version on our website][code].

[code]: https://vectorradio.ca/about/community-code-of-conduct/

## Volunteer Code of Conduct

Our pledge to our City colleagues on how we will behave.   

* [Volunteer Engagement Policy](https://policy.vancouver.ca/ADMIN028.pdf)
* [Volunteers Overview](https://app.betterimpact.com/Volunteer/Main/StoredDocument/60622)
* [2024](https://app.betterimpact.com/Volunteer/Main/StoredDocument/124473)

## EMBC Volunteer Code of Conduct

Our pledge to Emergency Management BC on how we will behave as part of the Public Safety Lifeline Volunteers.  

You may read the latest version [here][bccode] at the [EMBC website][pslv].  Learn about [forms][bcforms] and [policies][bcpol].  

* [Code of Conduct Policy](https://www2.gov.bc.ca/assets/gov/public-safety-and-emergency-services/emergency-preparedness-response-recovery/embc/policies/102_pslv_code_of_conduct_policy_aug_2016.pdf)
* [Code of Conduct Procedures](https://www2.gov.bc.ca/assets/gov/public-safety-and-emergency-services/emergency-preparedness-response-recovery/embc/policies/102_pslv_code_of_conduct_procedures_aug_2016.pdf)
* [Code of Conduct Frequent Questions](https://www2.gov.bc.ca/assets/gov/public-safety-and-emergency-services/emergency-preparedness-response-recovery/embc/policies/102_pslv_code_of_conduct_faqs_aug_2016.pdf)

[pslv]: https://www2.gov.bc.ca/gov/content/safety/emergency-preparedness-response-recovery/volunteers_

[bccode]: https://www2.gov.bc.ca/assets/gov/public-safety-and-emergency-services/emergency-preparedness-response-recovery/embc/policies/102_pslv_code_of_conduct_annex_-_code_of_conduct.pdf

[bcforms]: https://www2.gov.bc.ca/gov/content/safety/emergency-preparedness-response-recovery/volunteers/pslv-forms

[bcpol]: https://www2.gov.bc.ca/gov/content/safety/emergency-preparedness-response-recovery/emergency-management-bc/policies


## Confidentiality Agreement

Our pledge to our colleagues on how we will preserve the privacy of the people we help and the security of City assets.  

* [Confidentiality Agreement](VECTOR-ConfidentialityAgreement.pdf)
* [Latest Confidentiality Agreement Text](https://app.betterimpact.com/Volunteer/Main/StoredDocument/16591)
